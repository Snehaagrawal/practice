trigger MyAccount on Account (before insert,after insert,before update,after update) {
    system.debug('Trigger fired');
    list<contact> con1 = new list<contact>();
    list<Account> accRec=new list<Account>();
    if(Trigger.isInsert){
        
        if(Trigger.isBefore)
        {
            
            for( Account acc : Trigger.new){
                System.debug('we are in before insert'+acc );
                acc.AnnualRevenue=acc.Amount__c*12;
                
            }
            insert con1;
        }
        
        if(Trigger.isAfter)
        {
            system.debug('This is After');
            List<contact> conRec = new List<contact>();
            contact con;
            for(account acc: Trigger.new)
            {
                con = new contact();
                con.LastName=acc.Company_Name__c;
                con.Status__c='Draft';
                con.AccountId = acc.Id;
                conRec.add(con);
            }
            
            try{
                if(conRec.size() > 0){ //check for condition weather the list is empty or not 
                    insert conRec;
                }
            }
            catch(exception e)
            {
                System.debug('catch block'); 
            }
            
        }
    }
    if(Trigger.isUpdate)
    {
     if(Trigger.isAfter)
     {
       for(Account acc : Trigger.new)
       {
         if(acc.billingstreet != trigger.oldmap.get(acc.id).billingstreet ||
         acc.billingcity != trigger.oldmap.get(acc.id).billingcity ||
         acc.billingstate != trigger.oldmap.get(acc.id).billingstate ||
         acc.billingpostalcode != trigger.oldmap.get(acc.id).billingpostalcode ||
         acc.billingcountry != trigger.oldmap.get(acc.id).billingcountry) {
          accRec.add(acc);
       }
       }
     }
     for(contact c:[select id,accountid,phone,mailingstreet,mailingcity,mailingpostalcode,
     mailingcountry,mailingstate from contact where accountid in :trigger.new])
     {
        account a = trigger.newmap.get(c.accountid);
        if(c.mailingstreet == trigger.oldmap.get(c.accountid).billingstreet) {
        c.mailingstreet = a.billingstreet;
        
      }
      if(c.mailingcity == trigger.oldmap.get(c.accountid).billingcity) {
        c.mailingcity = a.billingcity;
       
      }
      if(c.mailingstate == trigger.oldmap.get(c.accountid).billingstate) {
        c.mailingstate = a.billingstate;
        
     }
      if(c.mailingpostalcode == trigger.oldmap.get(c.accountid).billingpostalcode) {
        c.mailingpostalcode = a.billingpostalcode;
        
      }
      if(c.mailingcountry == trigger.oldmap.get(c.accountid).billingcountry) {
        c.mailingcountry = a.billingcountry;
    
      }
        con1.add(c);
     }
   update con1;
  }
     }