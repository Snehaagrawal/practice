<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Start Here</description>
    <label>Force.com</label>
    <tab>standard-Chatter</tab>
    <tab>Employee__c</tab>
    <tab>Student__c</tab>
    <tab>Candidate__c</tab>
    <tab>Job_Application__c</tab>
    <tab>Employment_Website__c</tab>
    <tab>Data_Card__c</tab>
</CustomApplication>
