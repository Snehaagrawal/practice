<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>sla_update</fullName>
        <field>SLA__c</field>
        <literalValue>Gold</literalValue>
        <name>Update SLA Rating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Task Creation On New Record</fullName>
        <actions>
            <name>New_Account</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCity</field>
            <operation>equals</operation>
            <value>Bangalore</value>
        </criteriaItems>
        <description>Task Creation On New Record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update SLA Rating If Annual Revenue  Greater Than Equals To One Lakh</fullName>
        <actions>
            <name>sla_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.AnnualRevenue</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;INR 100,000&quot;</value>
        </criteriaItems>
        <description>Update SLA Rating If Annual Revenue Is Greater Than Equals To One Lakh.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>demo11</fullName>
        <actions>
            <name>New_Accountt</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>task2</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>demotesting</fullName>
        <actions>
            <name>test</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>testing</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>New_Account</fullName>
        <assignedTo>adm@demo.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>New account with city Bangalore is created. Contact Customer.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Create Task On Creation Of New Account With City Bangalore</subject>
    </tasks>
    <tasks>
        <fullName>New_Accountt</fullName>
        <assignedTo>adm@demo.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>New Accountt</subject>
    </tasks>
    <tasks>
        <fullName>test</fullName>
        <assignedTo>adm@demo.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>test</subject>
    </tasks>
</Workflow>
